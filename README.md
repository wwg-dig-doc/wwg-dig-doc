# Dokumentation der digitalen Schulinfrastruktur
## Städtisches Wim-Wenders-Gymnasium Düsseldorf

Dieses Repository dient zur Dokumentation der digitalen Infrastruktur des [Wim-Wenders-Gymnasiums](https://ww-gym.de/).
