# Achtung!!!
Die folgende Anleitung wird vervollständigt. Allerdings wird zeitgleich eine neuere Anleitung aufgesetzt, die eine modernere, leistungsfähigerer und besser skalierbare Lösung bietet. Beide Anleitungen werden hier zur Verfügung gestellt.

# Digitale Infrastruktur
Diese Anleitung hilft dabei einen Server aufzusetzten, der es einer Schule erlaubt Schulsoftware selbstständig zu verwalten.

## 1. Server vorbereiten

### 1. Wahl eines Anbieters
Das Wim-Wenders-Gymnasium hat mehrere Anforderungen an einen Server-Anbieter:

1. Server-Standort in Deutschland aus datenschutzrechtlichen Gründen.
2. Nach Möglichkeit sollen die Server mit Strom aus erneuerbaren Energien betrieben werden.
3. Komplette einfache Selbstverwaltung der Server.
4. Nach Möglichkeit ein etablierter Anbieter um Ausfallsicherheit zu maximieren.

Das Wim-Wenders-Gymnasium hat sich für [Hetzner](https://www.hetzner.com/) entschieden.
Ein "mittelgroßer" virtueller Server reicht nach unserer Erfahrung für die meisten Services für eine Schule mit rund 500 Mitgliedern.

Wir haben uns für einen Cloud-Server entschieden.
Diese sind leicht zu nutzen und bieten Vorteile bei der Verwaltung.
BigBlueButton (Videokonferenz-System) läft bei uns auf einem CPX41-Server (~30€).
Im Distanzunterricht hat sich erwiesen, dass dies etwas zu leistungsschwach ist.
Alle anderen Services (Moodle, Website, etc) sind in Containern auf einem CX41-Server (~19 €) installiert.
Ein großer Vorteil der Virtuellen Server bei Hetzner ist die Möglichkeit Speicherplatz dynamisch einzubinden.


### 2. Installation eines Betriebssystems

Wir nutzen auf beiden Servern die von Hetzner angebotenen Ubuntu-Images.
Für die Installation sind wir der offiziellen [Dokumentation](https://docs.bigbluebutton.org/) gefolgt.


### 3. Verwaltung & Sicherheit


### 4. Installation der benötigten Software

Für den Betrieb mehrerer Software-Services bietet sich im Einstieg [Docker](https://de.wikipedia.org/wiki/Docker_(Software)) an.
Dieses installieren wir nach der offizellen [Anleitung](https://docs.docker.com/engine/install/ubuntu/).


#### Installation von Docker Engine

 1. Zuerst müssen eventuell bestehende Versionen deinstalliert werden:

 ```
 sudo apt-get remove docker docker-engine docker.io containerd runc
 ```

 2. Es wird empfohlen das offizielle Docker-Repository für die Installation zu nutzen:

 ```
 sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
 ```

 3. Der offizielle GPG-Schlüssel muss importiert werden:

 ```
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
 ```

 4. Als nächstes muss das Repository zu den Paketquellen hinzu gefügt werden:

 ```
 sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```


### 5. Backup-Lösungen

## 2. Einzelne Services in Betrieb nehmen

### 1. Loadbalancer & Proxy


```linenums="1"
### Static ######################################################

DOMAINNAME=example.com
PUID=1000 # Die Nutzer-ID
PGID=999 # Die Gruppen-ID
TZ="Europe/Berlin" # Zeitzone
USERDIR="/mnt/Volume" # Pfad zur Partition
```

```yaml linenums="1"
version: "3"

### Networks ######################################################

networks:
  default:
    driver: bridge
  traefik:
    internal: true
#  mailcowdockerized_mailcow-network: ## Wird später für den Webmail-Service benötigt.
#    external: true

### Volumes ######################################################

volumes:
  libreoffice:



### Services ######################################################

services:

### Loadbalancer & Proxy ######################################################

## Docker-Proxy (Isoliert den Loadbalancer Traefik vom Docker-Socket um die Systemsicherheit zu erhöhen)
  dockerproxy:
    image: tecnativa/docker-socket-proxy:latest
    hostname: dockerproxy
    container_name: dockerproxy
    depends_on:
      - watchtower
    privileged: yes
    environment:
      CONTAINERS: 1
    networks:
      - traefik
    ports:
      - 2375
    volumes:
      - "/etc/localtime:/etc/localtime:ro" # Zeitzone
      - "/var/run/docker.sock:/var/run/docker.sock" # Docker-Socket

## Traefik-Proxy (Findet Services routet and loadbalancet sie)
  traefik:
    image: traefik:2.3
    hostname: traefik
    container_name: traefik
    depends_on:
      - dockerproxy
      - watchtower
    security_opt:
      - no-new-privileges:true
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.traefik.entrypoints=http"
      - "traefik.http.routers.traefik.rule=Host(`traefik.${DOMAINNAME}`)"
      - "traefik.http.middlewares.traefik-auth.basicauth.users=nutzer:Hash"
      - "traefik.http.routers.traefik.middlewares=https-redirect@file"
      - "traefik.http.routers.traefik-secure.entrypoints=https"
      - "traefik.http.routers.traefik-secure.rule=Host(`traefik.${DOMAINNAME}`)"
      - "traefik.http.routers.traefik-secure.tls=true"
      - "traefik.http.routers.traefik-secure.tls.certresolver=http"
      - "traefik.http.routers.traefik-secure.middlewares=traefik-auth,secHeaders@file"
      - "traefik.http.routers.traefik-secure.service=api@internal"
      - "providers.file.filename=/dynamic_conf.yml"
      - "providers.file.watch=true"
    networks:
      - default # Nur das “default”-Netzwerk ist direkt aus dem Internet erreichbar
      - traefik
    ports:
      - "80:80"
      - "443:443"
    restart: unless-stopped
    volumes:
      - "/etc/localtime:/etc/localtime:ro" # Zeitzone
      - "${USERDIR}/traefik/acme.json:/acme.json" # Zertifikate
      - "${USERDIR}/traefik/traefik.yml:/traefik.yml:ro" # statische Konfiguration
      - "${USERDIR}/traefik/dynamic_conf.yml:/dynamic_conf.yml:ro" # dynamische Konfiguration
```

### 2. Datenbank-Backup
### 3. Nutzer-Management-System
### 4. Single-Sign-On-System
### 5. Internetseite
### 6. Lernmanagementsystem
### 7. Webmail
### 8. File-Sharing
