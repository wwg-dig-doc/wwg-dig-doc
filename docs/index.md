# Willkommen in der digitalen Infrastruktur einer Schule

Die Digitalisierung unseres Alltags hat weitreichende Folgen für das Leben und Lernen unserer Schülerinnen und Schüler.
Kontinuierliche Änderungen in der digitalen Medienlandschaft versteht das Wim-Wenders-Gymnasium als Chance, sogenannte neue Medien sinnvoll in den Lernprozess der Schülerinnen und Schüler zu integrieren.
Medien wie Smartphones, elektrische Wiedergabegeräte und Computer werden von einem Großteil der Schüler und Schülerinnen täglich vielseitig genutzt.
Eine zentrale Aufgabe der Schule sollte es sein, diese Medienaffinität der Jugendlichen sinnvoll zu kanalisieren, produktiv zu nutzen und unterstützend zu begleiten.
Wir wollen unseren Schülerinnen und Schülern eine medienbezogene Reflexions- und Handlungskompetenz vermitteln.
Sie sollen lernen, die Mediensysteme kritisch zu bewerten, sicher zu nutzen und effektiv als Ressource für ihre individuelle Bildungsbiografie und Identitätsarbeit auszuschöpfen.
Dazu gehört explizit die Erziehung zu einem digital mündigen Teil der Gesellschaft, der die Regeln im gesellschaftlichen Miteinander nicht nur kennt, sondern auch lebt und schützt.
Eine besondere Rolle spielt am Wim-Wenders-Gymnasium die Verbindung der Naturwissenschaften mit den Künsten -- Die kreative Nutzung technische und digitaler Medien hat in unserer Schule somit einen großen Stellenwert.
Ziel ist es das digitale Miteinander über die Schulgrenzen hinaus aktiv zu Gestalten und dem Ruf gerecht zu werden, eine Schule des 21. Jahrhunderts zu sein.

Um das digitale Miteinander nicht nur zu ermöglichen sondern zu einer bereichernden Erfahrung zu machen plant das Wim-Wenders-Gymnasiums kontinuierlich Prozesse und Kommmunikation in allen Bereichen des Schulalltags zu verbessern.
Orientiert an agilen Konzepten aus der Wirtschaft werden Optimierungs-Potenziale identifiziert, Verbesserungsideen evaluiert und implementiert.
Idealerweise stehen am Ende eines jeden Implementationszykluses individuelle oder kollegiale Fortbildungen, die sicherstellen, dass die Lehrkräfte den angepassten Anforderungen gewachsen sind.
Auf diese Weise werden am Wim-Wenders-Gymnasium nicht nur digitale Lernplattformen kontinuierlich optimiert, sondern das gesamte digitale Miteinander.

Um eine digitale Infrastruktur bereit zu stellen betreibt das Wim-Wenders-Gymnasium einen virtuellen Server.
Es hat sich als flexibler, kosten- und wartungsärmer erwiesen, als ein physischer.
Die Auswahl des Anbieters folgte den Maßgaben: Standort in Deutschland, langjähriger Bestand, möglichst umweltfreundlicher Betrieb.
Finanziert wird werden die monatlichen Kosten vom Verein der „Freunde und Förderer des Gymnasiums an der Schmiedestraße“.
Kosten und Leistungen können regelmäßig neu verhandelt werden.

Administriert wird der virtuelle Schulserver von zwei Lehrerinnen und Lehrern (in Beratung externer Experten). Die Auswahl erfolgt nach diesen Kriterien: nachhaltiger Nutzen, Umsetzbarkeit, Kosten (nach Möglchkeit wird quelloffene, kostenfreie Software verwendet).
Folgende Kern-Services werden zur Verfügung gestellt:

## Projekt Überblick

* **Internetseite**: Der Web-Auftritt des Wim-Wenders-Gymnasiums ist über [ww-gym.de](https://ww-gym.de) erreichbar. Hier präsentiert sich die Schule in Form einer klassischen Internetseite. Der Auftritt ist somit ein wichtiges unidirektionales Kommunikationsmittel zur Außendarstellung der Schule. Inhaltlich wird die Seite von zwei Lehrerinnen und Lehrern betreut. Komplementiert wird die Außendarstellung durch einen Facebook-Auftritt, der von zwei weiteren Kolleginnen und Kollegen aktualisiert wird.
* **Lernmanagement-System**: Das schulinterne Lernmanagement-System (LMS) Moodle [moodle.ww-gym.de](https://moodle.ww-gym.de) bietet Lehrern und Schülerinnen und Schülern eine interaktive Lehr- und Lernumgebung. über die Plattform kann sowohl in, als auch außerhalb der Schule Unterricht stattfinden. Die Plattform ist somit ein zentraler Bestandteil des digitalen Lernens am Wim-Wenders-Gymnasium. Zusätzlich kann es eine wichtige Rolle in der Kommunikation innerhalb der Schulgemeinde spielen. Auch bietet die Moodle-Plattform die Möglichkeit Unterrichtsprozesse zu differenzieren und zu individualisieren. Verwaltet wird das LMS von zwei Lehrerinnen und Lehrern.
* **WebMail**: Das Wim-Wenders-Gymnasium stellt den Schülerinnen und Schülern eine E-Mail-Adresse zur Verfügung. Das Webmail-Portal erreichen die sie unter [ww-gym.de/SoGo](https://mail.ww-gym.de/SoGo). Die individuelle E-Mail-Adresse dient zur Kommunikation zwischen Lehrern und Schülern.
* **File-Sharing**: Der digitale Campus des Wim-Wenders-Gymnasiums wird durch die Software Nextcloud bereit gestellt. Unter [cloud.ww-gym.de](https://cloud.ww-gym.de) werden Dateien ausgetauscht, gespeichert und synchronisiert. Klassen speichern Lernprodukte, Lehrerinnen und Lehrer speichern Unterrichtsmaterial. Weitere Funktionen wie Projektmanagement und Integration in das LMS sind geplant. Administriert wird der Campus von zwei Lehrerinnen und Lehrern.
* **Agile Kommunikationsplattform**: Um eine agile Kommunikation zwischen Eltern, Schulteam und Schülern zu verbessern sollen unterschiedliche digitale Möglichkeiten getestet werden. Denkbar sind: Ticket-Systeme, Aula, etc. Hier ist eine der größten  Entwicklungspotentiale des Wim-Wenders-Gymnasiums versteckt. Der Wunsch der Schule ist es eine transparente Kommunikation auf Augenhöhe zwischen allen Beteiligten des Schulalltags zu schaffen. Digitale Werkzeuge (wie Aula) könnten diese Vision elegant verwirklichen und Entscheidungsprozesse noch demokratischer machen.

Weitere Bedarfe werden im Rahmen der agilen Schulkultur regelmäßig und kurzfristig evaluiert, so das die Software-Infrastruktur kontinuierlich wächst.
Im Rahmen einer offenen Kommmunikationskultur plant das Wim-Wenders-Gymnasium eine öffentliche Dokumentation der digitalen Infrastruktur um diese mit anderen Schulen zu teilen.
Die individuelle selbstgestaltete Softwarelandschaft orientiert an offenen Standards soll auch anderen Schulen die Möglichkeit bieten ihr digitales Miteinander zu gestalten.
Das Wim-Wenders-Gymnasium tritt mit Hilfe eines digitalen Depots in Austausch und will damit Schule machen.

### Rechtliches
Um rechtliche Rahmenvorgaben einzuhalten, werden die digitale Infrastruktur, sowie die Inhalte regelmäßig überprüft. In einem zweiten Schritt wird gegebenenfalls die Datenschutzerklärung des jeweiligen Angebots aktualisiert und transparent präsentiert.

## Ziel des Projekts

Dieses Repository soll die Server-Infrastruktur des Wim-Wenders-Gymnasium dokumentieren.
Ein Ziel ist durch eine übersichtliche Präsentation der einzelnen Soft- und Hardware-Komponenten eine Transparenz für die Nutzer zu schaffen (Lehrkräfte, Schülerinnen und Schüler sowie Eltern).
Die Präsentation schafft außerdem eine Nachhaltigkeit in dem Sinne, dass mit ihrer Hilfe andere Schulen eine kostengünstige, selbsverwaltete, zukunftssichere und skalierbare digitale Infrastruktur erstellen und verwalten können.
Die hier dargestellten Informationen können direkt als Anleitung genutzt werden.

## Digitale Infrastruktur

1. Server vorbereiten
   1. Wahl eines Anbieters
   2. Installation eines Betriebssystems
   3. Verwaltung & Sicherheit
   4. Installation der benötigten Software
   5. Backup-Lösungen
2. Einzelne Services in Betrieb nehmen
   1. [Loadbalancer & Proxy](https://wwg-dig-doc.gitlab.io/wwg-dig-doc/infrastructure/#1-loadbalancer-proxy)
   2. Datenbank-Backup
   3. Nutzer-Management-System
   4. Single-Sign-On-System
   5. Internetseite
   6. Lernmanagementsystem
   7. Webmail
   8. File-Sharing